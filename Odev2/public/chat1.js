$("#myModal").modal()


// Bağlantı Kurulumu
var socket = io.connect('http://localhost:3001');


// DOM
var mesaj = document.getElementById('mesaj'),
    kullaniciadi = document.getElementById('kullaniciadi'),
    kullanici_adiValue = document.getElementById('kullanici_adi'),
    btn = document.getElementById('gonder'),
    kullanicilar = document.getElementById('online_users');
    cikti = document.getElementById('cikti'),
    kullanici_yaziyor = document.getElementById('yaziyor'),




$('#myModal').on('hidden.bs.modal', function () {
    kullanici_adiValue.innerHTML = '<p>' + '<strong>' + 'Kullanıcı Adınız : ' + kullaniciadi.value + '</strong>';


    socket.emit('new user', kullaniciadi.value, function (data) {


    })


})


socket.on('get users', function (data) {

    kullanicilar.innerHTML = '';
    for (var i = 0; i < data.length; i++) {
        kullanicilar.innerHTML += '<li>' + data[i] + '</li>';

    }

})

// gönder butonuna tıkladğımızda olacak işlemler
btn.addEventListener('click', function () {
    socket.emit('chat-every', {
        mesaj: mesaj.value,
        mesaj_baslik: kullaniciadi.value,
    });
    /*MongoClient.connect(url,function (err,db) {
        var dtb=db.db("deneme")
        dtb=db.collection("kullanıcılar").insertOne({
            kadı : kullaniciadi.value,
            kmesaj : mesaj.value
        });

    });*/
    mesaj.value = "";
});

mesaj.addEventListener('keypress', function (e) {


    if (mesaj !== '') {

        socket.emit('yaziyor-1', {
            kullanici_adi : kullaniciadi.value,
        });
        var key = e.which || e.keyCode;
        if (key === 13) { // 13 is enter
            // code for enter
            socket.emit('chat-every', {
                mesaj: mesaj.value,
                mesaj_baslik: kullaniciadi.value,
            });
            mesaj.value = "";
        }
    }
    else {
        socket.emit('yaziyor-1', "");
    }


});

socket.on('chat-every', function (data) {
    kullanici_yaziyor.innerHTML = "";
    cikti.innerHTML += '<li >' + '<strong>' + data.mesaj_baslik + '</strong>' + ':' + data.mesaj + '</li>';

})

socket.on('yaziyor-1', function (data) {
    kullanici_yaziyor.innerHTML = '<p><em>' + data.kullanici_adi + ': mesaj yazıyor...' + '</em></p>'

})

