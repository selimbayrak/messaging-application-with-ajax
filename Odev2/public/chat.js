$("#myModal").modal()


// Bağlantı Kurulumu
var socket = io.connect('http://localhost:3001');


// DOM
var mesaj = document.getElementById('mesaj'),
    kullaniciadi = document.getElementById('kullaniciadi'),
    kullanici_adiValue = document.getElementById('kullanici_adi'),
    btn = document.getElementById('gonder'),
    cikti = document.getElementById('cikti'),
    kullanici_yaziyor = document.getElementById('yaziyor'),
    kullanicilar = document.getElementById('online_users');


//oda butonları
var oda_1 = document.getElementById('oda_1'),
    oda_2 = document.getElementById('oda_2'),
    oda_3 = document.getElementById('oda_3');

//


var odalar = ['oda_1', 'oda_2', 'oda_3'];

var seciliOda;

oda_1.addEventListener('click', function () {


    socket.emit('room', odalar[0]);
    seciliOda = odalar[0];

    kullanici_adiValue.innerHTML = '<p>' + '<strong>' + 'Kullanıcı Adınız : ' + kullaniciadi.value + '</strong>' + '  ' + '<p>' + '<strong>' + 'Şuanda bu gruptasınız : ' + seciliOda + '</p>' + '</strong>';

});
oda_2.addEventListener('click', function () {


    socket.emit('room', odalar[1]);
    seciliOda = odalar[1];

    kullanici_adiValue.innerHTML = '<p>' + '<strong>' + 'Kullanıcı Adınız : ' + kullaniciadi.value + '</strong>' + '  ' + '<p>' + '<strong>' + 'Şuanda bu gruptasınız : ' + seciliOda + '</p>' + '</strong>';

});
oda_3.addEventListener('click', function () {


    socket.emit('room', odalar[2]);
    seciliOda = odalar[2];

    kullanici_adiValue.innerHTML = '<p>' + '<strong>' + 'Kullanıcı Adınız : ' + kullaniciadi.value + '</strong>' + '  ' + '<p>' + '<strong>' + 'Şuanda bu gruptasınız : ' + seciliOda + '</p>' + '</strong>';

});
//


$('#myModal').on('hidden.bs.modal', function () {
    kullanici_adiValue.innerHTML = '<p>' + '<strong>' + 'Kullanıcı Adınız : ' + kullaniciadi.value + '</strong>' + '  ' + '<p>' + '<strong>' + 'Şuanda bu gruptasınız : ' + seciliOda + '</p>' + '</strong>';


    socket.emit('new user', kullaniciadi.value, function (data) {


    })


})


//emit events
//mesaj göndermek için gerekli komut

// gönder butonuna tıkladğımızda olacak işlemler
btn.addEventListener('click', function () {
    socket.emit('chat', {
        mesaj: mesaj.value,
        mesaj_baslik: kullaniciadi.value,
        oda: seciliOda
    });
    mesaj.value = "";

});

mesaj.addEventListener('keypress', function (e) {


    if (mesaj !== '') {

        socket.emit('yaziyor', {
            kullanici_adi : kullaniciadi.value,
            oda: seciliOda
        });
        var key = e.which || e.keyCode;
        if (key === 13) { // 13 is enter
            // code for enter
            socket.emit('chat', {
                mesaj: mesaj.value,
                mesaj_baslik: kullaniciadi.value,
                oda: seciliOda
            });
            mesaj.value = "";
        }
    }
    else {
        socket.emit('yaziyor', "");
    }


});


//Listen for events sunucudan gönderilen mesajları dinliyoruz
//burada eğer sunucudan bir mesaj gelirse front-end de yapılacak işlemleri yapıyoruz
socket.on('connection', function (socket) {
    baglantilar.push(socket);
    console.log('Bağlantı Sayısı: %s', baglantilar.length);

})

socket.on('chat', function (data) {
    kullanici_yaziyor.innerHTML = "";
     cikti.innerHTML += '<li >' + '<strong>' + data.mesaj_baslik + '</strong>' + ':' + data.mesaj + '</li>';



})

socket.on('yaziyor', function (data) {
    kullanici_yaziyor.innerHTML = '<p><em>' + data.kullanici_adi + ': mesaj yazıyor...' + '</em></p>'

})


socket.on('disconnect', function (socket) {
    baglantilar.splice(baglantilar.indexOf(socket), 1);
    console.log('Bağlantı Sayısı: %s', baglantilar.length);


})

socket.on('get users', function (data) {

    kullanicilar.innerHTML = '';
    for (var i = 0; i < data.length; i++) {
        kullanicilar.innerHTML += '<li>' + data[i] + '</li>';

    }

})