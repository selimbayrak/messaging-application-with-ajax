var express = require('express');
var socket = require('socket.io');
//var MongoClient = require('mongodb').MongoClient;
//var url = "mongodb://localhost:27017/";

var kullanicilar = [];
var baglantilar = [];
var kullanici = [];

// App setup
var app = express();
var server = app.listen(3001, function () {
    //console.log("Port:3001 dinleniyor... http://localhost:3001")
});

// Static Files
app.use(express.static('public'));

var people={};


//Socket Setup
var io = socket(server);

//io.on bağlantı açıldığı zaman yapılacak işlemler buranın altında olur
io.on('connection', function (socket) {

    // socket.join('everyone');

    baglantilar.push(socket);
    console.log('Bağlantı Sayısı: %s', baglantilar.length);

    //baglanan kullanıcıların id lerini de gösterir.

    //oluşturduğumuz socket üzerinden bilgileri sunucu tarafında alıyoruz
    socket.on('chat', function (data) {
        // io.sockets.emit('chat', data);


        io.to(data.oda).emit('chat',data)


    });

    socket.on('yaziyor', function (data) {


        if (data === "") {
            socket.to(data.oda).broadcast.emit('yaziyor', data)
        } else {
            socket.to(data.oda).broadcast.emit('yaziyor', data)
        }

    });

    socket.on('disconnect', function () {

        kullanicilar.splice(kullanicilar.indexOf(socket.username), 1)
        updateUsernames();

        console.log('Kullanıcı Ayrıldı:' + socket.username);
        baglantilar.splice(baglantilar.indexOf(socket), 1);
        console.log('Bağlantı Sayısı: %s', baglantilar.length);




    });

    socket.on('new user', function (data, callback) {


        console.log('Bağlanan Kullanici:' + data)
        callback(true);
        socket.username = data;
        kullanicilar.push(socket.username);
        updateUsernames();
    })

    function updateUsernames() {
        io.sockets.emit('get users', kullanicilar)

    }

    socket.on('room', function (myroom) {
        console.log(socket.username + ',' + ' "' + myroom + '" odasına katıldı.')

        socket.join(myroom);
    });

    socket.on('chat-every',function (msg) {
        io.emit('chat-every', msg);
    })

    socket.on('yaziyor-1', function (data) {

        if (data === "") {
            socket.broadcast.emit('yaziyor-1', data)
        } else {
            socket.broadcast.emit('yaziyor-1', data)
        }

    });

    socket.on('chat-private',function (data) {
        socket.join(data.id);
        io.to(data.id).emit('chat-private',data)
    })

})

module.exports = app;
